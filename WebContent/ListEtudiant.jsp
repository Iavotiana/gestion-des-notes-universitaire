<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="ramaroson.Etudiant" %>
 <%
 	ServletContext context = this.getServletContext();
 	String connecter=(String)session.getAttribute("connecter");
 	Boolean sess = Boolean.valueOf(connecter);
 	Etudiant[] etudiant=(Etudiant[]) request.getAttribute("etudiant");
 	float[] moyenne=(float[]) request.getAttribute("moyenne");
 	String[] mention=(String[]) request.getAttribute("mention");
 	String[] classe=(String[]) context.getAttribute("classes");
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/index.css">
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<title>Liste Etudiant</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="row">
				<div class="col-md-2" ><a href="/notes-etudiants/ServletListeEtudiants">Liste Etudiant</a></div>
				<% if(sess){ %>
				<div class="col-md-2" ><a href="InsertEtudiant.jsp">Insert Etudiant</a></div>
				<div class="col-md-2" ><a href="/notes-etudiants/Formulaire_note_Servelet">Insert notes</a></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ><a href="/notes-etudiants/ServletDeconnect">Deconnection</a></div>
				<% } else{ %>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ><a href="index.jsp">Login</a></div>
				<% } %>
			</div>
		</div>
		<div class="titre" >Liste des Etudiants</div>
		<div class="row">
			<div class="col-md-1" ></div>
			<div class="col-md-10" >
				<table class="table table-sm">
				  <thead>
				    <tr>
				      <th scope="col">Nom et prenoms</th>
				      <th scope="col">Niveau</th>
				      <th scope="col">Moyenne</th>
				      <th scope="col">Mention</th>
				      <th scope="col"></th>
				      <th scope="col"></th>
				    </tr>
				  </thead>
				  <tbody>
				  	<% for(int i = 0; i<etudiant.length; i++){ %>
				     <tr>
				      <td><%= etudiant[i].getNom()+" "+etudiant[i].getPrenom() %></td>
				      <td><%= etudiant[i].getClasse() %></td>
				      <td><%= moyenne[i] %></td>
				      <td><%= mention[i] %></td>
				      <% if(sess){ %>
					      <td><div class="edite"><a href="InsertEtudiant.jsp?update=<%=etudiant[i].getId()%>"><span class="fa fa-edit" ></span></a> </div></td>
					      <td><div class="delete"><a href="/notes-etudiants/ServletDeleteEtudiants.jsp?id=<%=etudiant[i].getId()%>"><span class="fa fa-close" ></span></a></div></td>
					   <%} %>
					    <td><a href="/notes-etudiants/Liste_Note_Etudiant.html?idEtudiant=<%=etudiant[i].getId()%>">Note</a></td>
				    </tr>
				    <% } %>
				  </tbody>
				</table>
				<br>
				<nav aria-label="Page navigation example">
				  <ul class="pagination justify-content-center">
				    	<% for(int i=0; i<classe.length; i++) { %>
				    	<li class="page-item"><a class="page-link" href="/notes-etudiants/ServletListeEtudiants.jsp?classe=<%= classe[i] %>"><%= classe[i] %></a></li>
				    	<% } %>
				  </ul>
				</nav>
			</div>	
			<div class="col-md-1" ></div>
		</div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>