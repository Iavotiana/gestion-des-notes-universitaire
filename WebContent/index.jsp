<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/index.css">
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<title>Login Enseignant</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="row">
				<div class="col-md-2" ><a href="/notes-etudiants/ServletListeEtudiants">Liste Etudiants</a></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4" ></div>
			<div class="col-md-4" >
				<div class="icon_login">
					<span class="fa fa-user" ></span>
				</div>
				<div  class="contener_login">
					<div  class="containt_login">
						<form action="/notes-etudiants/Traitement_Login_Servlet" method="post">
						  <div class="form-group">
						    <label for="formGroupExampleInput">Nom</label>
						    <input type="text" class="form-control" id="formGroupExampleInput" name="nom">
						  </div>
						  <div class="form-group">
						    <label for="formGroupExampleInput2">Mots de passe</label>
						    <input type="password" class="form-control" id="formGroupExampleInput2" name="mdp">
						  </div>
						  <button type="submit" class="btn btn-success">Valider</button>
						  <%if(request.getParameter("erreur")!=null){ %>
							  	<div class="alert alert-danger" role="alert">
								  Mots de passe ou nom incorrecte!
								</div>
							<%} %>
						</form>
					 </div>
				</div>
			</div>
			<div class="col-md-4" ></div>		
		</div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>