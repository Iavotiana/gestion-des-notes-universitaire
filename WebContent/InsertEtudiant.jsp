<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import = "ramaroson.Etudiant" %>
<%
 	ServletContext context = this.getServletContext();
	String[] classe=(String[]) context.getAttribute("classes");
	Etudiant etd=null;
	if(request.getParameter("update")!=null){
		int id=Integer.parseInt(request.getParameter("update"));
		etd=(Etudiant)new Etudiant().findById(id);
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/insertion.css">
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<link rel="icon" type="MIT/png" href="assets/image/favicon.png">
<title>Insertion etudiant</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="row">
				<div class="col-md-2" ><a href="/notes-etudiants/ServletListeEtudiants">Liste Etudiant</a></div>
				<div class="col-md-2" ><a href="InsertEtudiant.jsp">Insert Etudiant</a></div>
				<div class="col-md-2" ><a href="/notes-etudiants/Formulaire_note_Servelet">Insert notes</a></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ><a href="/notes-etudiants/ServletDeconnect">Deconnection</a></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2" ></div>
			<div class="col-md-8" >
				<div class="icon_login">
					<% if(request.getParameter("update")!=null){ %>
						<center><span class="fa fa-user" > Update etudiant</span></center>
					<%}else{ %>
						<center><span class="fa fa-user" > Nouvelle etudiant</span></center>
					<%} %>
					
				</div>
				<div  class="contener_login">
					<div  class="containt_login">
						<% if(request.getParameter("update")!=null){ %>
							<form action="/notes-etudiants/ServletUpdateEtudiants" method="get">
						<%}else{ %>
							<form action="/notes-etudiants/ServletInsertEtudiants" method="post">
						<%} %>
						  <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Nom</label>
						    <div class="col-sm-10">
						    	<% if(request.getParameter("update")!=null){ %>
						      		<input type="text" class="form-control" id="colFormLabel" name="nom" value="<%= etd.getNom() %>" placeholder="<%= etd.getNom()  %>">
						      	<%}else{ %>
						      		<input require name="nom" type="text" class="form-control" id="colFormLabel" name="nom">
						      	<%} %>	
						    </div>
						  </div>
						 <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Prenom(s)</label>
						    <div class="col-sm-10">
						    	<% if(request.getParameter("update")!=null){ %>
						      		<input require type="text" class="form-control" id="colFormLabel" name="prenom" value="<%=etd.getPrenom()%>" placeholder="<%=etd.getPrenom() %>">
						      	<%}else{ %>
						      		<input require name="prenom" type="text" class="form-control" id="colFormLabel" name="nom">
						      	<%} %>	
						    </div>
						  </div>
						   <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Date de naissance</label>
						    <div class="col-sm-10">
						    	<% if(request.getParameter("update")!=null){ %>
						      		<input require type="date" class="form-control" id="colFormLabel" name="dateNaissance" value="<%=etd.getDateNaissance() %>">
						      	<%}else{ %>
						      		<input require name="dateNaissance" type="date" class="form-control" id="colFormLabel">
						      	<%} %>	
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Sexe</label>
						    <div class="col-sm-10">
						    	<% if(request.getParameter("update")!=null){ %>
						      		<input require type="text" class="form-control" id="colFormLabel" name="sexe" value="<%= etd.getSexe() %>" placeholder="<%=etd.getSexe() %>">
						      	<%}else{ %>
						      		<input require name="sexe" type="text" class="form-control" id="colFormLabel">
						      	<%} %>	
						    </div>
						  </div>
						  <div class="form-group">
      
						  <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Niveau</label>
						    <div class="col-sm-10">
						      <select require name="classe" class="form-control">
								<% for(int i = 0; i<classe.length; i++){ %>
								 	<% if(request.getParameter("update")!=null && classe[i].compareTo(etd.getClasse())==0){ %>
										<option value="<%=classe[i]  %>" selected><%=classe[i] %></option>
									<%}else{ %>	
										<option value="<%=classe[i]  %>"><%=classe[i] %></option>
									<% } %>
								<%} %> 
							  </select>
							  <% if(request.getParameter("update")!=null){ %>
									<input require name="id" type="hidden" value="<%= etd.getId()%>">
								<%} %>
						    </div>
						  </div><br>
						  <button type="submit" class="btn btn-success">Inserer</button>
						</form>
					 </div>
				</div>
			</div>
			<div class="col-md-2" ></div>		
		</div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>