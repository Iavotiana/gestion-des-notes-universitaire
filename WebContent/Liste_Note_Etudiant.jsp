<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import = "iavotiana.Affichage_Note" %>
<%	Affichage_Note[] tabEtu = (Affichage_Note[]) request.getAttribute("etudiant");
	String connecter=(String)session.getAttribute("connecter");
	Boolean sess = Boolean.valueOf(connecter);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/index.css">
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<title>List Note</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="row">
				<div class="col-md-2" ><a href="/notes-etudiants/ServletListeEtudiants">Liste Etudiant</a></div>
				<% if(sess){ %>
				<div class="col-md-2" ><a href="InsertEtudiant.jsp">Insert Etudiant</a></div>
				<div class="col-md-2" ><a href="/notes-etudiants/Formulaire_note_Servelet">Insert notes</a></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ><a href="/notes-etudiants/ServletDeconnect">Deconnection</a></div>
				<% } else{ %>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ><a href="index.jsp">Login</a></div>
				<% } %>
			</div>
		</div>
	<br>
		<div class="row">
			<div class="col-md-1" ></div>
			<div class="col-md-10" >
				<div class="card text-center">
				  <div class="card-header">
				    RESULTAT
				  </div>
				  <div class="card-body">
				    <h5 class="card-title">
				    	<div class="titre" > <p class="card-text"><%= tabEtu[0].getNom() +" "+ tabEtu[0].getPrenoms() %></p>
				    <%if(tabEtu[0].getClasse()!=null){ %>	
				    	<p class="card-text">Niveau:<%= tabEtu[0].getClasse() %></p></div>
				    <%} %>	
				    </h5>
				    <table class="table table-sm">
				  <thead>
				  	
				    <tr>
				      <th scope="col">Matiere</th>
				      <th scope="col">Niveau</th>
				      <th scope="col">Coeff</th>
				      <th scope="col">Note</th>
				      <th scope="col"></th>
				      <th scope="col"></th>
				    </tr>
				  </thead>
				  <%if(tabEtu[0].getClasse()!=null){ %>
				  <tbody>
				  	 <%for(int i = 0; i<tabEtu.length; i++){ %>
				     <tr>
				      <td><%= tabEtu[i].getMatiere() %></td>
				      <td><%= tabEtu[i].getClasse() %></td>
				      <td><%= tabEtu[i].getCoeff() %></td>
				      <td><%= tabEtu[i].getNote() %></td>
				       <% if(sess){ %>
					      <td><div class="edite"><a href="/notes-etudiants/Formulaire_Update.html?idNote=<%= tabEtu[i].getIdNote() %>"><span class="fa fa-edit" ></span></a> </div></td>
					      <td><div class="delete"><a href="/notes-etudiants/Delete_Note.html?idNote=<%= tabEtu[i].getIdNote() %>&idEtudiant=<%= tabEtu[i].getIdEtudiant() %>" ><span class="fa fa-close" ></span></a></div></td>
					   <%} %>
				    </tr>
				    <% } %>
				  </tbody>
				  <%} %>
				</table>
				  </div>
				</div>
			</div>	
			<div class="col-md-1" ></div>
		</div>
	</div>
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>