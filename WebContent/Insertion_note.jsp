<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import = "ramaroson.Matiere,ramaroson.Etudiant" %>
<% Object[] tabMat = (Object[]) request.getAttribute("matiere");
	Object[] tabEtu = (Object[]) request.getAttribute("etudiant");
	String[] classe= new Etudiant().getListeClasse(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/insertion.css">
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<link rel="icon" type="MIT/png" href="assets/image/favicon.png">
<title>Insertion</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="row">
				<div class="col-md-2" ><a href="/notes-etudiants/ServletListeEtudiants">Liste Etudiant</a></div>
				<div class="col-md-2" ><a href="InsertEtudiant.jsp">Insert Etudiant</a></div>
				<div class="col-md-2" ><a href="/notes-etudiants/Formulaire_note_Servelet">Insert notes</a></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ></div>
				<div class="col-md-2" ><a href="/notes-etudiants/ServletDeconnect">Deconnection</a></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2" ></div>
			<div class="col-md-8" >
				<div class="icon_login">
					<span class="fa fa-pencil" > Inserer Note</span>
				</div>
				<div  class="contener_insertion_Note">
					<div  class="containt_login">
						<form action="/notes-etudiants/Insertion_note_Servelet" method="post">
						  <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Etudiant</label>
						    <div class="col-sm-10">
						      <select require class="form-control" name="idEtudiant">
								  <%for(int i = 0; i<tabEtu.length; i++){
									Etudiant etudiant = (Etudiant)tabEtu[i]; %>
									<option value="<%= etudiant.getId() %>"><%= etudiant.getNom() %></option>
								<% } %>
							  </select>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Niveau</label>
						    <div class="col-sm-10">
						      <select require class="form-control" name="classe">
								  <%for(int i = 0; i<classe.length; i++){	 %>
									<option value="<%= classe[i] %>"><%= classe[i] %></option>
								<% } %>
							  </select>
						    </div>
						  </div>
						   <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Matiere</label>
						    <div class="col-sm-10">
						      <select require class="form-control" name="idMatiere">
								  <%for(int i = 0; i<tabMat.length; i++){
									Matiere matiere = (Matiere)tabMat[i]; %>
									<option value="<%= matiere.getId() %>"><%= matiere.getNom() %></option>
								  <% } %>
							  </select>
						    </div>
						  </div>
						   <div class="form-group row">
						    <label for="colFormLabel" class="col-sm-2 col-form-label">Note</label>
						    <div class="col-sm-10">
						      <input require type="text" class="form-control" id="colFormLabel" name="note">
						    </div>
						  </div>
						  <br>
						  <button type="submit" class="btn btn-success"><span class="fa fa-save" > Enregistrer</span></button>
						</form>
					 </div>
				</div>
			</div>
			<div class="col-md-2" ></div>		
		</div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>