package iavotiana;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Insertion_note_Servelet
 */
public class Insertion_note_Servelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insertion_note_Servelet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			int idEtudiant = Integer.parseInt(request.getParameter("idEtudiant"));
			int idMatiere = Integer.parseInt(request.getParameter("idMatiere"));
			int note = Integer.parseInt(request.getParameter("note"));
			String classe = request.getParameter("classe");
			Note newNote = new Note(idEtudiant,idMatiere,note,classe);
			newNote.save();
			response.sendRedirect("/notes-etudiants/Liste_Note_Etudiant.html?idEtudiant="+idEtudiant);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
