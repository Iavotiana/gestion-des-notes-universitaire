package iavotiana;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Liste_Note_Etudiant
 */
public class Liste_Note_Etudiant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Liste_Note_Etudiant() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Affichage_Note[] tabEtudiant=null;
		try {
			String idEtudiant = request.getParameter("idEtudiant");
			int idEtu = Integer.parseInt(idEtudiant);
			tabEtudiant = new Affichage_Note().findNoteEtudiant(idEtu);
			request.setAttribute("etudiant", tabEtudiant);
			RequestDispatcher dispat = request.getRequestDispatcher("Liste_Note_Etudiant.jsp");
			dispat.forward(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
