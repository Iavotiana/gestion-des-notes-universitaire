package iavotiana;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import connection.MysqlCon;

public class Affichage_Note {
	int idEtudiant;
	String nom;
	String prenoms;
	int idMatiere;
	String matiere;
	int note;
	int coeff;
	int idNote;
	public int getIdNote() {
		return idNote;
	}
	public void setIdNote(int idNote) {
		this.idNote = idNote;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	String classe;
	public int getIdEtudiant() {
		return idEtudiant;
	}
	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenoms() {
		return prenoms;
	}
	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}
	public int getIdMatiere() {
		return idMatiere;
	}
	public void setIdMatiere(int idMatiere) {
		this.idMatiere = idMatiere;
	}
	public String getMatiere() {
		return matiere;
	}
	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public int getCoeff() {
		return coeff;
	}
	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
	public Affichage_Note() {
		// TODO Auto-generated constructor stub
		
	}
	public Affichage_Note(int idEtudiant, String nom, String prenoms, int idMatiere, int note, String matiere, int coeff,String classe, int idNote) {
		this.idEtudiant = idEtudiant;
		this.nom = nom;
		this.prenoms = prenoms;
		this.idMatiere = idMatiere;
		this.note = note;
		this.matiere = matiere;
		this.coeff = coeff;
		this.classe = classe;
		this.idNote = idNote;
	}
	public Affichage_Note[] findAll() throws Exception{
		Affichage_Note[] tab= null;
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `Note_Etudiant`");
            res=pst.executeQuery();
            while(res.next()){taille++;}
            tab = new Affichage_Note[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int idEtudiant = res.getInt(1);
            	String Nom = res.getString(2);
            	String Prenoms = res.getString(3);
            	int idMatiere = res.getInt(4);
            	int note1 = res.getInt(5);
            	String matiere = res.getString(6);
            	int coeff = res.getInt(7);
            	String classe = res.getString(8);
            	int idNote = res.getInt(9);
            	tab[i]= new Affichage_Note(idEtudiant,Nom, Prenoms, idMatiere,note1,matiere,coeff,classe,idNote);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return tab;
	}
	public Affichage_Note[] findNoteEtudiant(int idEtudiant1) throws Exception{
		Affichage_Note[] tab= null;
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		try{
			con.setAutoCommit(false);
			pst = con.prepareStatement("SELECT * FROM `Note_Etudiant` where idEtudiant=?");
			pst.setInt(1, idEtudiant1);
            res=pst.executeQuery();
            while(res.next()){taille++;}
            tab = new Affichage_Note[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int idEtudiant = res.getInt(1);
            	String Nom = res.getString(2);
            	String Prenoms = res.getString(3);
            	int idMatiere = res.getInt(4);
            	int note1 = res.getInt(5);
            	String matiere = res.getString(6);
            	int coeff = res.getInt(7);
            	String classe = res.getString(8);
            	int idNote = res.getInt(9);
            	tab[i]= new Affichage_Note(idEtudiant,Nom, Prenoms, idMatiere,note1,matiere,coeff,classe,idNote);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return tab;
	}
	public Affichage_Note[] findNoteMatiere(int idMatiere1) throws Exception{
		Affichage_Note[] tab= null;
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `Note_Etudiant` where idMatiere="+idMatiere1);
            res=pst.executeQuery();
            while(res.next()){taille++;}
            tab = new Affichage_Note[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int idEtudiant = res.getInt(1);
            	String Nom = res.getString(2);
            	String Prenoms = res.getString(3);
            	int idMatiere = res.getInt(4);
            	int note1 = res.getInt(5);
            	String matiere = res.getString(6);
            	int coeff = res.getInt(7);
            	String classe = res.getString(8);
            	int idNote = res.getInt(9);
            	tab[i]= new Affichage_Note(idEtudiant,Nom, Prenoms, idMatiere,note1,matiere,coeff,classe,idNote);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return tab;
	}
	public Affichage_Note findNote(int idNote1) throws Exception{
		Affichage_Note tab= null;
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `Note_Etudiant` where idNote="+idNote1);
            res=pst.executeQuery();
            while(res.next()){
            	int idEtudiant = res.getInt(1);
            	String Nom = res.getString(2);
            	String Prenoms = res.getString(3);
            	int idMatiere = res.getInt(4);
            	int note1 = res.getInt(5);
            	String matiere = res.getString(6);
            	int coeff = res.getInt(7);
            	String classe = res.getString(8);
            	int idNote = res.getInt(9);
            	tab= new Affichage_Note(idEtudiant,Nom, Prenoms, idMatiere,note1,matiere,coeff,classe,idNote);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return tab;
	}
}
