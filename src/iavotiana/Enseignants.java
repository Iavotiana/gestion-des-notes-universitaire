package iavotiana;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import connection.MysqlCon;
import models.ModelPersonne;

public class Enseignants extends ModelPersonne{
	String mdp;

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public Enseignants(int id, String nom, String prenom, String sexe, String mdp) {
		super(id, nom, prenom, sexe);
		this.mdp = mdp;
	}
	public Enseignants() {
	}

		@Override
	public void save() throws Exception {
		// TODO Auto-generated method stub
			Connection con=new MysqlCon().Connect();
			PreparedStatement stat=null;
			try{
				con.setAutoCommit(false);
	            stat = con.prepareStatement("INSERT INTO `Enseignant`(`idEnseignant`, `Nom`, `Prenoms`, `Sexe`, `mdp`) VALUES (NUll,?,?,?,?)");
	            stat.setString(1,getNom());
	            stat.setString(2, getPrenom());
	            stat.setString(3, getSexe());
	            stat.setString(4, getMdp());
	            stat.executeUpdate();
	            con.commit();
	        }catch(Exception ex){   
	            con.rollback();
	            throw ex;
	        }finally{
	            if(stat!=null)stat.close();
	            if(con!=null)con.close();            
	        }     
	}

	@Override
	public void update() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement stat=null;
		try{
			con.setAutoCommit(false);
            stat = con.prepareStatement("UPDATE `enseignant` SET `Nom`=?,`Prenoms`=?,`Sexe`=?,`Mdp`=? WHERE idEnseignant=?");
            stat.setString(1,getNom());
            stat.setString(2, getPrenom());
            stat.setString(3, getSexe());
            stat.setString(4, getMdp());
            stat.setInt(5,getId());
            stat.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(stat!=null)stat.close();
            if(con!=null)con.close();            
        }     		
	}

	@Override
	public void remove() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement stat=null;
		try{
			con.setAutoCommit(false);
            stat = con.prepareStatement("DELETE FROM `enseignant` WHERE idEnseignant=?");
            stat.setInt(1,getId());
            stat.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(stat!=null)stat.close();
            if(con!=null)con.close();            
        }     
	}

	@Override
	public Object[] findAll() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		Enseignants[] enseignant =null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `enseignant`");
            res=pst.executeQuery();
            while(res.next()){taille++;}
            enseignant= new Enseignants[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int id = res.getInt(1);
            	String nom = res.getString(2);
            	String prenom = res.getString(3);
            	String sexe = res.getString(4);
            	String mdp = res.getString(5);
            	enseignant[i]=new Enseignants(id,nom,prenom,sexe,mdp);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return enseignant;
	}

	@Override
	public Object findById(int id1) throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		Enseignants enseignant=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `enseignant` where idEnseignant=?");
            pst.setInt(1, id1);
            res=pst.executeQuery();
            while(res.next()){
            	int id2 = res.getInt(1);
            	String nom = res.getString(2);
            	String prenom = res.getString(3);
            	String sexe = res.getString(4);
            	String mdp = res.getString(5);
            	enseignant =new Enseignants(id2,nom,prenom,sexe,mdp);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return enseignant;
	}
	public Object findByNom(String nom1) throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		Enseignants enseignant=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `enseignant` where nom=?");
            pst.setString(1, nom1);
            res=pst.executeQuery();
            while(res.next()){
            	int id2 = res.getInt(1);
            	String nom = res.getString(2);
            	String prenom = res.getString(3);
            	String sexe = res.getString(4);
            	String mdp = res.getString(5);
            	enseignant =new Enseignants(id2,nom,prenom,sexe,mdp);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return enseignant;
	}
	public String getSha1(String mdp) throws Exception{
		String retour= null;
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT SHA1('"+mdp+"')");
            res=pst.executeQuery();
            while(res.next()){
            	retour = res.getString(1);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return retour;
	}
	public boolean valider_Login(String mdpEntrer) throws Exception{
		Boolean retour = false;
		String mdp1= "prof"+mdpEntrer;
		String mdpSha1 = this.getSha1(mdp1);
		if(mdpSha1.compareTo(this.getMdp())==0){
			retour = true;
		}
		return retour;
	}
}
