package iavotiana;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ramaroson.Etudiant;
import ramaroson.Matiere;

/**
 * Servlet implementation class Formulaire_Update
 */
public class Formulaire_Update extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Formulaire_Update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			String idNote = request.getParameter("idNote");
			int id = Integer.parseInt(idNote);
			Affichage_Note note = (Affichage_Note) new Affichage_Note().findNote(id);
			request.setAttribute("note", note);
			Object[] tabMat = new Matiere().findAll();
			request.setAttribute("matiere", tabMat);
			Object[] tabEtudiant = new Etudiant().findAll();
			request.setAttribute("etudiant", tabEtudiant);
			RequestDispatcher dispat = request.getRequestDispatcher("/Update_Note.jsp");
			dispat.forward(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
