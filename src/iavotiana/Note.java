package iavotiana;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import connection.MysqlCon;
import models.ModelBase;

/**
 * @author Iavotiana
 *
 */
public class Note extends ModelBase{
	int idEtudiant;
	int idMatiere;
	int note;
	String classe;
	
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public int getIdEtudiant() {
		return idEtudiant;
	}
	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}
	public int getIdMatiere() {
		return idMatiere;
	}
	public void setIdMatiere(int idMatiere) {
		this.idMatiere = idMatiere;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public Note(int idEtudiant, int idMatiere, int note, String classe) {
		this.idEtudiant = idEtudiant;
		this.idMatiere = idMatiere;
		this.note = note;
		this.classe = classe;
	}
	public Note(int id, int idEtudiant, int idMatiere, int note, String classe) {
		super(id);
		this.idEtudiant = idEtudiant;
		this.idMatiere = idMatiere;
		this.note = note;
		this.classe = classe;
	}
	public Note() {
	}
	@Override
	public void save() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement stat=null;
		try{
			con.setAutoCommit(false);
            stat = con.prepareStatement("INSERT INTO `Note`(`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES (NUll,?,?,?,?)");
            stat.setInt(1,getIdEtudiant());
            stat.setInt(2, getIdMatiere());
            stat.setInt(3, getNote());
            stat.setString(4, getClasse());
            stat.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(stat!=null)stat.close();
            if(con!=null)con.close();            
        }     
	}
	@Override
	public void update() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement stat=null;
		try{
			con.setAutoCommit(false);
            stat = con.prepareStatement("UPDATE `Note` SET `idEtudiant`=?,`idMatiere`=?,`note`=?, `classe`=? WHERE idNote=?");
            stat.setInt(1,getIdEtudiant());
            stat.setInt(2, getIdMatiere());
            stat.setInt(3, getNote());
            stat.setString(4, getClasse());
            stat.setInt(5, this.getId());
            stat.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(stat!=null)stat.close();
            if(con!=null)con.close();            
        }     		
	}
	@Override
	public void remove() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement stat=null;
		try{
			con.setAutoCommit(false);
            stat = con.prepareStatement("DELETE FROM `Note` WHERE idNote=?");
            stat.setInt(1,this.getId());
            stat.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(stat!=null)stat.close();
            if(con!=null)con.close();            
        }     
	}
	@Override
	public Object[] findAll() throws Exception {
		// TODO Auto-generated method stub
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		Note[] note =null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `Note`");
            res=pst.executeQuery();
            while(res.next()){taille++;}
            note = new Note[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int id = res.getInt(1);
            	int idEtudiant = res.getInt(2);
            	int idMatiere = res.getInt(3);
            	int note1 = res.getInt(4);
            	String classe = res.getString(5);
            	note[i]=new Note(id,idEtudiant,idMatiere,note1,classe);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return note;
	}
	@Override
	public Object findById(int id) throws Exception {
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		Note nt=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `note` where idNote=?");
            pst.setInt(1, id);
            res=pst.executeQuery();
            while(res.next()){
            	int idN= res.getInt(1);
            	int idE=res.getInt(2);
            	int idM=res.getInt(3);
            	int note=res.getInt(4);
            	String classe = res.getString(5);
            	nt=new Note(idN,idE,idM,note,classe);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return nt;
	}
}
