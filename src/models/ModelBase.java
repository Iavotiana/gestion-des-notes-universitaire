package models;

public abstract class ModelBase {
	int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ModelBase(int id) {
		super();
		this.id = id;
	}
	public ModelBase(){
	}
	public abstract void save() throws Exception;
	
	public abstract void update()throws Exception;
	
	public abstract void remove()throws Exception;
	
	public abstract Object[] findAll()throws Exception;
	
	public abstract Object findById(int id)throws Exception;
	
}
