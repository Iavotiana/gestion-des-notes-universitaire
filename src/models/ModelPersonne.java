package models;

public abstract class ModelPersonne {
	int id;
	String nom;
	String prenom;
	String sexe;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public ModelPersonne(int id, String nom, String prenom, String sexe) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
	}
	public ModelPersonne(){
	}
	public abstract void  save () throws Exception;
	
	public abstract void update() throws Exception;
	
	public abstract void remove()throws Exception;
	
	public abstract Object[] findAll()throws Exception;
	
	public abstract Object findById(int d)throws Exception;
}
