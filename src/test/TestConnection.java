package test;
import connection.MysqlCon;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TestConnection {

	public static void main(String[] args) throws Exception{
		MysqlCon sql= new MysqlCon();
		Connection con=null;
		Statement stmt=null;
		ResultSet rs=null;
		try{
			con = sql.Connect();
			stmt=con.createStatement();  
			rs=stmt.executeQuery("select * from matiere");  
			while(rs.next())  
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getInt(3)); 
		}catch(Exception e){e.getMessage();}
		finally{
			con.close(); 
			rs.close();
			stmt.close();
		}		
	}
}
