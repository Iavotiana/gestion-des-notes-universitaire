package ramaroson;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import connection.MysqlCon;
import models.ModelPersonne;

public class Etudiant extends ModelPersonne{
	String dateNaissance;
	String classe;
	public String getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	
	public Etudiant(int id, String nom, String prenom, String sexe,String date,String classe) {
		super(id, nom, prenom, sexe);
		this.dateNaissance=date;
		this.classe=classe;
	}
	public Etudiant(){
	}
	@Override
	public void save() throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("INSERT INTO `etudiant`(`idEtudiant`, `Nom`, `Prenoms`, `DateNaissance`, `Sexe`, `Classe`) VALUES (NUll,?,?,?,?,?)");
            pst.setString(1,getNom());
            pst.setString(2, getPrenom());
            pst.setString(3, getDateNaissance()); 
            pst.setString(4, getSexe());
            pst.setString(5, getClasse());
            pst.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }     
	}
	@Override
	public void update() throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("UPDATE `etudiant` SET `idEtudiant`=?,`Nom`=?,`Prenoms`=?,`DateNaissance`=?,`Sexe`=?,`Classe`=? WHERE idEtudiant=?");
            pst.setInt(1,getId());
            pst.setString(2,getNom());
            pst.setString(3, getPrenom());
            pst.setString(4, getDateNaissance()); 
            pst.setString(5, getSexe());
            pst.setString(6, getClasse());
            pst.setInt(7,getId());
            pst.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }     		
	}
	@Override
	public void remove() throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("DELETE FROM `etudiant` WHERE idEtudiant=?");
            pst.setInt(1,getId());
            pst.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }     
	}
	@Override
	public Object[] findAll() throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		Etudiant[] etd=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `etudiant`");
            res=pst.executeQuery();
            while(res.next()){taille++;}
            etd= new Etudiant[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int id= res.getInt(1);
            	String nom=res.getString(2);
            	String prenom=res.getString(3);
            	String sexe=res.getString(5);
            	String date=res.getString(4);
            	String classe=res.getString(6);
            	etd[i]=new Etudiant(id,nom,prenom,sexe,date,classe);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return etd;
	}
	@Override
	public Object findById(int id) throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		Etudiant etd=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `etudiant` where idEtudiant=?");
            pst.setInt(1, id);
            res=pst.executeQuery();
            while(res.next()){
            	int idE= res.getInt(1);
            	String nom=res.getString(2);
            	String prenom=res.getString(3);
            	String sexe=res.getString(5);
            	String date=res.getString(4);
            	String classe=res.getString(6);
            	etd=new Etudiant(idE,nom,prenom,sexe,date,classe);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return etd;
	}
	public float getMoyenneGeneral(int id) throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		float val=0;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT moyenne FROM `moyenne_general` where idEtudiant=?");
            pst.setInt(1, id);
            res=pst.executeQuery();
            while(res.next()){
            	val=res.getFloat(1);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return val;
	}
	public String getMention(int id) throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		String val=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT mention FROM `moyenne_general` where idEtudiant=?");
            pst.setInt(1, id);
            res=pst.executeQuery();
            while(res.next()){
            	val=res.getString(1);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return val;
	}
	public String[] getListeClasse() throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		String[] val=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT DISTINCT classe from etudiant");
            res=pst.executeQuery();
            int j=0;
            while(res.next()){
            	j++;
            }
            val=new String[j];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	val[i]=res.getString(1);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return val;
	}
	public Etudiant[] findByClasse(String classe) throws Exception{
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		Etudiant[] etd=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `etudiant`where classe=?");
            pst.setString(1, classe);
            res=pst.executeQuery();
            while(res.next()){taille++;}
            etd= new Etudiant[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int id= res.getInt(1);
            	String nom=res.getString(2);
            	String prenom=res.getString(3);
            	String sexe=res.getString(5);
            	String date=res.getString(4);
            	String classes=res.getString(6);
            	etd[i]=new Etudiant(id,nom,prenom,sexe,date,classes);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return etd;
	}
}
