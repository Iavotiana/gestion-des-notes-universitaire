package ramaroson;

import ramaroson.Etudiant;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletListeEtudiants
 */
public class ServletListeEtudiants extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListeEtudiants() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Etudiant[] etudiant=null;
		try {
			ServletContext context = this.getServletContext();
			String[] classe=new Etudiant().getListeClasse();	
			if(request.getParameter("classe")==null){
				etudiant = new Etudiant().findByClasse(classe[0]);
			}
			else{
				etudiant = new Etudiant().findByClasse(request.getParameter("classe"));
			}
			float[] moyenne=new float[etudiant.length];
			String[] mention=new String[etudiant.length];
			for(int i=0;i<etudiant.length;i++){
				moyenne[i]=new Etudiant().getMoyenneGeneral(etudiant[i].getId());
				mention[i]=new Etudiant().getMention(etudiant[i].getId());
				if(mention[i]==null){mention[i]=" ";}
			}	
			request.setAttribute("etudiant", etudiant);
			request.setAttribute("moyenne", moyenne);
			request.setAttribute("mention", mention);
			context.setAttribute("classes", classe);
			RequestDispatcher dispat = request.getRequestDispatcher("/ListEtudiant.jsp");
			dispat.forward(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
