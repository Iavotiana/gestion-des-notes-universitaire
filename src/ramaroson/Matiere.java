package ramaroson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import connection.MysqlCon;
import models.ModelBase;

public class Matiere  extends ModelBase{
  String nom;
  int coeff;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getCoeff() {
		return coeff;
	}
	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
	public Matiere(int id, String nom, int coeff) {
		super(id);
		this.nom = nom;
		this.coeff = coeff;
	}
	public Matiere() {
	}
	public void save() throws Exception {
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("INSERT INTO `matiere`(`idMatiere`, `Nom`, `Coeff`) VALUES (NUll,?,?)");
            pst.setString(1,getNom());
            pst.setInt(2, getCoeff());
            pst.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }     	
	}
	@Override
	public void update() throws Exception {
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("UPDATE `matiere` SET `idMatiere`=?,`Nom`=?,`Coeff`=? WHERE idMatiere=?");
            pst.setInt(1,getId());
            pst.setString(2,getNom());
            pst.setInt(3, getCoeff());
            pst.setInt(4,getId());
            pst.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }     				
	}
	@Override
	public void remove() throws Exception {
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("DELETE FROM `matiere` WHERE idMatiere=?");
            pst.setInt(1,getId());
            pst.executeUpdate();
            con.commit();
        }catch(Exception ex){   
            con.rollback();
            throw ex;
        }finally{
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }     
	}
	@Override
	public Object[] findAll() throws Exception {
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		int taille=0;
		Matiere[] mtr=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `matiere`");
            res=pst.executeQuery();
            while(res.next()){taille++;}
            mtr= new Matiere[taille];
            res.close();
            res=pst.executeQuery();
            int i=0;
            while(res.next()){
            	int id= res.getInt(1);
            	String nom=res.getString(2);
            	int coeff=res.getInt(3);
            	mtr[i]=new Matiere(id,nom,coeff);
            	i++;
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return mtr;
	}
	@Override
	public Object findById(int id) throws Exception {
		Connection con=new MysqlCon().Connect();
		PreparedStatement pst=null;
		ResultSet res=null;
		Matiere mtr=null;
		try{
			con.setAutoCommit(false);
            pst = con.prepareStatement("SELECT * FROM `matiere` where idMatiere=?");
            pst.setInt(1, id);
            res=pst.executeQuery();
            while(res.next()){
            	int idM= res.getInt(1);
            	String nom=res.getString(2);
            	int coeff=res.getInt(3);
            	mtr=new Matiere(idM,nom,coeff);
            }
        }catch(Exception ex){   
            throw ex;
        }finally{
        	if(res!=null)res.close();
            if(pst!=null)pst.close();
            if(con!=null)con.close();            
        }
		return mtr;
	}
}
