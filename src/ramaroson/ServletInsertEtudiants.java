package ramaroson;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletInsertEtudiants
 */
public class ServletInsertEtudiants extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletInsertEtudiants() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String nom=request.getParameter("nom");
			String prenom=request.getParameter("prenom");
			String sexe=request.getParameter("sexe");
			String classe=request.getParameter("classe");
			String date=request.getParameter("dateNaissance");
			Etudiant etd=new Etudiant(0,nom,prenom,sexe,date,classe);
			etd.save();
			response.sendRedirect("/notes-etudiants/ServletListeEtudiants.jsp?classe="+classe);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}

}
