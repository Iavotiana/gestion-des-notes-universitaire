package ramaroson;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletUpdateEtudiants
 */
public class ServletUpdateEtudiants extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletUpdateEtudiants() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			int  id=Integer.parseInt(request.getParameter("id"));
			String nom=request.getParameter("nom");
			String prenom=request.getParameter("prenom");
			String sexe=request.getParameter("sexe");
			String classe=request.getParameter("classe");
			String date=request.getParameter("dateNaissance");
			Etudiant etd=new Etudiant(id,nom,prenom,sexe,date,classe);
			etd.update();
			response.sendRedirect("/notes-etudiants/ServletListeEtudiants.jsp?classe="+classe);
		}catch(Exception e){
			e.getMessage();
		}

	}

}
