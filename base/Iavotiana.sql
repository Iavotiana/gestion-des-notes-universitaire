--reset auto_increment ALTER TABLE tablename AUTO_INCREMENT = 1
INSERT INTO `enseignant` (`idEnseignant`, `Nom`, `prenoms`, `sexe`, `mdp`) VALUES (NULL, 'Helene', 'Marie', 'f', SHA1('profmarie')),
 (NULL, 'Bertrand', 'Francois', 'm', SHA1('profrancois')), 
 (NULL, 'Didier', 'Jacque', 'm', SHA1('profjacque')), 
 (NULL, 'Mathilde', 'Lafonte', 'f', SHA1('profmarie')) ;

		
create or replace view Note_Etudiant as select etudiant.idEtudiant, 
		etudiant.Nom, 
		etudiant.Prenoms, Note.idMatiere, 
							Note.note,
										matiere.nom as matiere, 
										matiere.coeff ,
										Note.classe,
										Note.idNote
		from etudiant left join Note on etudiant.idEtudiant=note.idEtudiant
		left join Matiere on note.idMatiere=matiere.idMatiere order by idEtudiant;
		
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '1', '1', '14', 'S1'), 
(NULL, '1', '2', '11', 'S1'), 
(NULL, '1', '3', '16', 'S1'), 
(NULL, '1', '4', '06', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '2', '1', '10', 'S1'), 
(NULL, '2', '2', '06', 'S1'), 
(NULL, '2', '3', '14', 'S1'), 
(NULL, '2', '4', '09', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '3', '1', '04', 'S1'), 
(NULL, '3', '2', '10', 'S1'), 
(NULL, '3', '3', '05', 'S1'), 
(NULL, '3', '4', '06', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '4', '1', '10', 'S1'), 
(NULL, '4', '2', '14', 'S1'), 
(NULL, '4', '3', '05', 'S1'), 
(NULL, '4', '4', '11', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '5', '1', '17', 'S1'), 
(NULL, '5', '2', '12', 'S1'), 
(NULL, '5', '3', '14', 'S1'), 
(NULL, '5', '4', '06', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '6', '1', '06', 'S1'), 
(NULL, '6', '2', '15', 'S1'), 
(NULL, '6', '3', '08', 'S1'), 
(NULL, '6', '4', '10', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '7', '1', '10', 'S1'), 
(NULL, '7', '2', '12', 'S1'), 
(NULL, '7', '3', '14', 'S1'), 
(NULL, '7', '4', '09', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '8', '1', '12', 'S1'), 
(NULL, '8', '2', '11', 'S1'), 
(NULL, '8', '3', '14', 'S1'), 
(NULL, '8', '4', '06', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '9', '1', '05', 'S1'), 
(NULL, '9', '2', '11', 'S1'), 
(NULL, '9', '3', '10', 'S1'), 
(NULL, '9', '4', '06', 'S1') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '10', '1', '10', 'S1'), 
(NULL, '10', '2', '11', 'S1'), 
(NULL, '10', '3', '14', 'S1'), 
(NULL, '10', '4', '08', 'S1') ;


INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '11', '1', '09', 'S2'), 
(NULL, '11', '2', '20', 'S2'), 
(NULL, '11', '3', '16', 'S2'), 
(NULL, '11', '4', '08', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '12', '1', '06', 'S2'), 
(NULL, '12', '2', '14', 'S2'), 
(NULL, '12', '3', '15', 'S2'), 
(NULL, '12', '4', '08', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '13', '1', '14', 'S2'), 
(NULL, '13', '2', '12.5', 'S2'), 
(NULL, '13', '3', '13', 'S2'), 
(NULL, '13', '4', '10', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '14', '1', '10.5', 'S2'), 
(NULL, '14', '2', '14', 'S2'), 
(NULL, '14', '3', '10', 'S2'), 
(NULL, '14', '4', '06', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '15', '1', '9.5', 'S2'), 
(NULL, '15', '2', '12', 'S2'), 
(NULL, '15', '3', '13', 'S2'), 
(NULL, '15', '4', '10', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '16', '1', '14', 'S2'), 
(NULL, '16', '2', '16', 'S2'), 
(NULL, '16', '3', '10', 'S2'), 
(NULL, '16', '4', '08', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '17', '1', '10', 'S2'), 
(NULL, '17', '2', '11', 'S2'), 
(NULL, '17', '3', '14', 'S2'), 
(NULL, '17', '4', '08', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '18', '1', '07', 'S2'), 
(NULL, '18', '2', '13', 'S2'), 
(NULL, '18', '3', '10', 'S2'), 
(NULL, '18', '4', '06', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '19', '1', '14', 'S2'), 
(NULL, '19', '2', '16', 'S2'), 
(NULL, '19', '3', '11', 'S2'), 
(NULL, '19', '4', '06', 'S2') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '20', '1', '12.5', 'S2'), 
(NULL, '20', '2', '11', 'S2'), 
(NULL, '20', '3', '12', 'S2'), 
(NULL, '20', '4', '06', 'S2') ;

INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '21', '1', '12', 'S3'), 
(NULL, '21', '2', '11', 'S3'), 
(NULL, '21', '3', '12', 'S3'), 
(NULL, '21', '4', '06', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '22', '1', '11', 'S3'), 
(NULL, '22', '2', '12', 'S3'), 
(NULL, '22', '3', '10', 'S3'), 
(NULL, '22', '4', '09', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '23', '1', '14', 'S3'), 
(NULL, '23', '2', '16', 'S3'), 
(NULL, '23', '3', '08', 'S3'), 
(NULL, '23', '4', '05', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '24', '1', '09', 'S3'), 
(NULL, '24', '2', '06', 'S3'), 
(NULL, '24', '3', '10', 'S3'), 
(NULL, '24', '4', '08', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '25', '1', '12', 'S3'), 
(NULL, '25', '2', '11', 'S3'), 
(NULL, '25', '3', '12', 'S3'), 
(NULL, '25', '4', '06', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '26', '1', '10', 'S3'), 
(NULL, '26', '2', '04', 'S3'), 
(NULL, '26', '3', '12', 'S3'), 
(NULL, '26', '4', '06', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '27', '1', '14', 'S3'), 
(NULL, '27', '2', '10', 'S3'), 
(NULL, '27', '3', '17', 'S3'), 
(NULL, '27', '4', '09', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '28', '1', '17', 'S3'), 
(NULL, '28', '2', '14', 'S3'), 
(NULL, '28', '3', '10', 'S3'), 
(NULL, '28', '4', '13', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '29', '1', '04', 'S3'), 
(NULL, '29', '2', '15', 'S3'), 
(NULL, '29', '3', '12', 'S3'), 
(NULL, '29', '4', '10', 'S3') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '30', '1', '06', 'S3'), 
(NULL, '30', '2', '12', 'S3'), 
(NULL, '30', '3', '10', 'S3'), 
(NULL, '30', '4', '09', 'S3') ;


INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '31', '1', '10', 'S4'), 
(NULL, '31', '2', '15', 'S4'), 
(NULL, '31', '3', '13', 'S4'), 
(NULL, '31', '4', '08', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '32', '1', '12', 'S4'), 
(NULL, '32', '2', '05', 'S4'), 
(NULL, '32', '3', '10', 'S4'), 
(NULL, '32', '4', '12', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '33', '1', '12', 'S4'), 
(NULL, '33', '2', '14', 'S4'), 
(NULL, '33', '3', '11', 'S4'), 
(NULL, '33', '4', '09', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '34', '1', '10', 'S4'), 
(NULL, '34', '2', '12', 'S4'), 
(NULL, '34', '3', '02', 'S4'), 
(NULL, '34', '4', '10', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '35', '1', '09', 'S4'), 
(NULL, '35', '2', '08', 'S4'), 
(NULL, '35', '3', '13', 'S4'), 
(NULL, '35', '4', '12', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '36', '1', '18', 'S4'), 
(NULL, '36', '2', '10', 'S4'), 
(NULL, '36', '3', '09', 'S4'), 
(NULL, '36', '4', '12', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '37', '1', '12', 'S4'), 
(NULL, '37', '2', '13', 'S4'), 
(NULL, '37', '3', '13', 'S4'), 
(NULL, '37', '4', '10', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '38', '1', '14', 'S4'), 
(NULL, '38', '2', '06', 'S4'), 
(NULL, '38', '3', '04', 'S4'), 
(NULL, '38', '4', '12', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '39', '1', '15', 'S4'), 
(NULL, '39', '2', '10', 'S4'), 
(NULL, '39', '3', '12', 'S4'), 
(NULL, '39', '4', '11', 'S4') ;
INSERT INTO `note` (`idNote`, `idEtudiant`, `idMatiere`, `note`, `classe`) VALUES 
(NULL, '40', '1', '10', 'S4'), 
(NULL, '40', '2', '11', 'S4'), 
(NULL, '40', '3', '09', 'S4'), 
(NULL, '40', '4', '12', 'S4') ;