--View--
create view moyenne as select idEtudiant , sum(note*coeff)/sum(coeff) as moyenne from note join matiere 
	where note.idMatiere = matiere.idMatiere
	group by idEtudiant;
		
create or replace view moyenne_general as select moyenne.*,case 
	when moyenne >= 10 and moyenne < 12 then 'Passable ' 
	when moyenne >= 12 and moyenne <14 then 'Assez-bien'
	when moyenne >= 14 and moyenne <16 then 'Bien'
	when moyenne >= 16 then 'Tres-bien'
	else 'Ajour' end as mention
 from moyenne group by  idEtudiant, mention;

--reset auto_increment ALTER TABLE tablename AUTO_INCREMENT = 1
INSERT INTO `etudiant` (`idEtudiant`, `Nom`, `Prenoms`, `DateNaissance`, `Sexe`, `Classe`) VALUES (NULL, 'Martin', 'Enzo', '1999-08-14', 'm', 'S1') ;
INSERT INTO `etudiant` (`idEtudiant`, `Nom`, `Prenoms`, `DateNaissance`, `Sexe`, `Classe`) VALUES (NULL, 'Bernard', 'Assia', '2000-04-30', 'f', 'S1'), 
(NULL, 'Dubois', 'Mathis', '1999-02-26', 'm', 'S1'), 
(NULL, 'Thomas', 'Nina', '2001-09-31', 'f', 'S1'), 
(NULL, 'Robert', 'Nathan', '1999-06-17', 'm', 'S1'), 
(NULL, 'Richard', 'Chaima', '2001-04-17', 'f', 'S1'), 
(NULL, 'Petit', 'Axel', '2000-12-04', 'm', 'S1'), 
(NULL, 'Durand', 'Yasmine', '1999-03-16', 'f', 'S1'), 
(NULL, 'Leroy', 'Adrien', '2001-07-19', 'm', 'S1'), 
(NULL, 'Moreau', 'Clea', '2000-11-09', 'f', 'S1');
INSERT INTO `etudiant` (`idEtudiant`, `Nom`, `Prenoms`, `DateNaissance`, `Sexe`, `Classe`) VALUES (NULL, 'Morel', 'Matteo', '2000-05-17', 'm', 'S2'), 
(NULL, 'Girard', 'Leane', '2001-11-15', 'f', 'S2'), 
(NULL, 'Andre', 'Nicola', '1998-03-26', 'm', 'S2'), 
(NULL, 'Lefevre', 'Kenza', '2000-12-29', 'f', 'S2'), 
(NULL, 'Gabriel', 'Manon', '2001-02-16', 'm', 'S2'), 
(NULL, 'Mercier', 'Lily', '2002-10-25', 'f', 'S2'), 
(NULL, 'Dupont', 'Julien', '2000-09-17', 'm', 'S2'), 
(NULL, 'Lambert', 'Emma', '2000-01-01', 'f', 'S2'), 
(NULL, 'Bonnet', 'Leo', '2001-05-27', 'm', 'S2'), 
(NULL, 'Francois', 'Lea', '2001-09-13', 'f', 'S2') ;
INSERT INTO `etudiant` (`idEtudiant`, `Nom`, `Prenoms`, `DateNaissance`, `Sexe`, `Classe`) VALUES (NULL, 'Martinez', 'Mael', '2001-09-17', 'm', 'S3'), 
(NULL, 'Nicolas', 'Louis', '2000-07-04', 'm', 'S3'), 
(NULL, 'Herve', 'Clemence', '2001-02-07', 'f', 'S3'), 
(NULL, 'Perrin', 'Raphael', '2001-03-29', 'm', 'S3'), 
(NULL, 'Morin', 'Maeva', '1999-09-18', 'f', 'S3'), 
(NULL, 'Romain', 'Mathieu', '1998-09-06', 'm', 'S3'), 
(NULL, 'Clement', 'Lena', '2000-10-12', 'f', 'S3'), 
(NULL, 'Gauthier', 'Paul', '2000-08-14', 'm', 'S3'), 
(NULL, 'Dumont', 'Pauline', '1999-08-25', 'f', 'S3'), 
(NULL, 'Lopez', 'Theo', '2000-09-19', 'm', 'S3');
INSERT INTO `etudiant` (`idEtudiant`, `Nom`, `Prenoms`, `DateNaissance`, `Sexe`, `Classe`) VALUES (NULL, 'Fontaine', 'Laura', '2001-09-28', 'f', 'S4'), 
(NULL, 'Chevalier', 'Tom', '2000-10-10', 'm', 'S4'), 
(NULL, 'Duval', 'Augustin', '2001-09-10', 'm', 'S4'), 
(NULL, 'Joly', 'Louis', '2001-08-13', 'm', 'S4'), 
(NULL, 'Gautier', 'Arnaud', '2000-10-20', 'm', 'S4'), 
(NULL, 'Roche', 'Aaron', '1999-05-16', 'm', 'S4'), 
(NULL, 'Noel', 'Manon', '2002-09-06', 'f', 'S4'), 
(NULL, 'Antonin', 'Lucas', '2000-14-07', 'm', 'S4'), 
(NULL, 'Romane', 'Manu', '2000-11-22', 'm', 'S4'), 
(NULL, 'Jean', 'Diego', '2000-04-14', 'm', 'S4');

INSERT INTO `Matiere` (`idMatiere`, `Nom`, `Coeff`) VALUES (NULL,'INF 210/211',10);
INSERT INTO `Matiere` (`idMatiere`, `Nom`, `Coeff`) VALUES (NULL,'INF 312',6);
INSERT INTO `Matiere` (`idMatiere`, `Nom`, `Coeff`) VALUES (NULL,'MATH 204',10);
INSERT INTO `Matiere` (`idMatiere`, `Nom`, `Coeff`) VALUES (NULL,'INF 209',4);


